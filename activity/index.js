const students = [];

function addStudent(name){
	students.push(name);
	console.log(name + " was added to the student list");
	console.log("Students List: ", students);
}

addStudent("Adrian");
addStudent("Joseph");
addStudent("Aidan");
addStudent("Byrne");
addStudent("Juan");
addStudent("Cheska");

function countStudents(){
	console.log("Total number of students: ", students.length);
}

countStudents();

console.log("List of students:");
function printStudents(){
	students.forEach(function(name){
		students.sort();
		console.log(name);
	})
}

printStudents();