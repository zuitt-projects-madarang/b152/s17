// Storing multiple values using variables

let student1 = '2020-01-17';
let student2 = '2020-01-20';
let student3 = '2020-01-25';
let student4 = '2020-02-25';

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);

// Storing multiple values in an array

const studentNumbers = ['2020-01-17', '2020-01-20', '2020-01-25', '2020-02-25']; //an array of strings
console.log(studentNumbers);

// Outputs the value of the element in the index 1 of the studentNumbers array
// All arrays starts with index 0
// Syntax: arrayName[index of value we want to output]
console.log(studentNumbers[1]);
console.log("Index 3 of studentNumbers: ", studentNumbers[3]);
console.log(studentNumbers[4]); //returns undefined since there is no declared value for the fourth index of the studentNumbers array

// Arrays are declared using the square brackets aka Array Literals
// Each data stored inside an array is called an array element
const emptyArray = []; //declares an empty array
const grades = [75, 85.5, 92, 94]; //an array of numbers
const computerBrands = ["Acer", "Asus", "Lenovo", "Apple", "Redfox", "Gateway"];

console.log("Empty Array Sample: ", emptyArray);
console.log(grades);
console.log("Computer Brands: ", computerBrands);

// All elements inside an array should have the same data type
// All elements inside an array should be related with each other

const mixedArr = [12, "Asus", undefined, null, {}]; //not ideal

const fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log("Value of fruits array before push()", fruits);

// push() function adds an element at the end of an array
fruits.push("Mango");
console.log("Value of fruits array after push()", fruits);

// Will return an error since we cannot assign the const variable fruits to another value
// fruits = "hello"

console.log("Value of fruits before pop()", fruits);

// Removes the last element in the array
fruits.pop();
console.log("Value of fruits after pop()", fruits);

console.log("Value of fruits before unshift()", fruits);

// unshift() adds one or more elements at the beginning of the array
fruits.unshift("Strawberry"); //adds Strawberry to the beginning of the fruits array
console.log("Values of fruits after unshift()", fruits);
fruits.unshift("Banana", "Raisin");
console.log(fruits);

// shift() removes the first element in our array

let removedFruit = fruits.shift(); //expected output is banana is removed from the fruits array
console.log("You successfully removed the fruit: ", removedFruit);
console.log(fruits);

// reverse() reverses the order of elements in an array
fruits.reverse();
console.log("Value of fruits after reverse()", fruits);

const tasks = [
	"drink html",
	"eat JS",
	"inhale CSS",
	'bake sass'
];

console.log("Values of tasks before sort()", tasks);
tasks.sort();
console.log("Values of tasks after sort()", tasks);

const oddNumbers = [1, 3, 9, 5, 7];

// Arranges the elements in alphanumeric sequence
console.log("Value of oddNumbers before sort()", oddNumbers);
oddNumbers.sort();
console.log("Value of oddNumbers after sort()", oddNumbers);


const countries = ["US", "PH", "CAN", "SG", "PH"];
// indexOf() finds the index of a given element where it is FIRST found
let indexCountry = countries.indexOf("PH");
console.log("Index of PH: ", indexCountry); // 1

// lastIndexOf() finds the index of a given element where it is LAST found
let lastIndexCountry = countries.lastIndexOf("PH");
console.log("Last Instance of PH: ", lastIndexCountry); // 4

console.log(countries);

// toString() converts an array into a single value that is separated by a comma
console.log(countries.toString());


const subTasksA = ["drink html", "eat JS"];
const subTasksB = ["inhale css", "breathe sass"];
const subTasksC = ["get git", "be node"];

// concat() joins 2 or more arrays
const subTasks = subTasksA.concat(subTasksB, subTasksC);
console.log(subTasks);

// join() converts an array into a single value, and is separated by a specified character
console.log(subTasks.join(" - "));
console.log(subTasks.join(" @ "));
console.log(subTasks.join(" x "));

const users = ["blue", "alexis", "bianca", "nikko", "adrian"];

console.log(users[0]); //blue
console.log(users[1]); //alexis

/*
	Syntax: 
	arrayName.forEach(function(eachElement){
		console.log(eachElement)
	})
*/

// forEach iterates each element inside an array
// We pass each element as the parameter of the function declared inside the forEach()

// We have a users array
// We want to iterate the users array using the forEach function
// Each element is stored inside the variable called user
// Logged in the browser the value of each user / each element inside the users array
users.forEach(function(user){
	//console.log("Name of Student: ", user);
	if (user == "blue"){
		console.log("Name of Student: ", user);
	}
})


/*Mini-Activity*/

const numberList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 287, 428, 420];

console.log("Even numbers:")
numberList.forEach(function(num){
	if (num % 2 === 0){
		console.log(num, " is an even number.");
	} else {
		console.log(num, " is an odd number.");
	}
})

// length property returns the number of elements inside an array
console.log(numberList.length); //13

// map() iterates each element and returns a new array depending on the result of the function's operation
// map() is useful when we will manipulate/change elements inside our array
// map() allows us to not touch/manipulate the original array
const numbersData = numberList.map(function(number){
	return number * number;
})

console.log(numbersData);
console.log(numberList); //not manipulated

// Multi-dimenstional arrays
// Useful for storing complex data structures

const chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"]
]
console.log(chessBoard); //logs the whole chessBoard array
console.log(chessBoard[0]); //logs the value in the index 0 of the chessBoard array
console.log(chessBoard[0][1]); //specific element inside the array in index 0